//
//  IntroCellView.swift
//  TimerLoop (iOS)
//
//  Created by Lorenzo Lewis on 12/27/20.
//

import SwiftUI

struct IntroCellView: View {

    let titleText: String
    let bodyText: String
    let icon: Image
    let color: Color

    var body: some View {
        HStack(alignment: .top) {
            icon
                .resizable()
                .frame(width: 30, height: 30, alignment: .center)
                .padding()
            VStack(alignment: .leading) {
                Text(titleText)
                    .font(.title2)
                    .padding(.bottom, 1)
                Text(bodyText)
                    .font(.subheadline)
                    .foregroundColor(Color(.label))
            }
        }
        .foregroundColor(color)
    }
}

struct IntroCellView_Previews: PreviewProvider {
    static var previews: some View {
        IntroCellView(
            titleText: "Days of the week",
            bodyText: "Choose a single day for your loop to repeat, only on weekdays, or every day of the week.",
            icon: Image(systemName: "calendar"),
            color: Color.accentColor)
    }
}
