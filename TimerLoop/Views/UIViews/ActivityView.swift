//
//  ActivityView.swift
//  TimerLoop (iOS)
//
//  Created by Lorenzo Lewis on 12/6/20.
//

import SwiftUI

/// Converts a UIActivityViewController into a view that can be displayed via .sheet in SwiftUI.
struct ActivityView: UIViewControllerRepresentable {
    
    let vc: UIActivityViewController
    
    func makeUIViewController(context: Context) -> UIActivityViewController {
        return vc
    }
    
    func updateUIViewController(_ uiViewController: UIActivityViewController, context: Context) {}
}
