//
//  MailView.swift
//  TimerLoop (iOS)
//
//  Created by Lorenzo Lewis on 12/6/20.
//

import SwiftUI
import MessageUI

// Adopted from https://www.hackingwithswift.com/books/ios-swiftui/using-coordinators-to-manage-swiftui-view-controllers

/// Converts a MFMailComposeViewController into a view that can be displayed via .sheet in SwiftUI.
struct MailView: UIViewControllerRepresentable, Identifiable {

    @Environment(\.presentationMode) var presentationMode

    public let id = UUID()
    let vc: MFMailComposeViewController
    
    class Coordinator: NSObject, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate {

        var parent: MailView
        
        init(_ parent: MailView) {
            self.parent = parent
        }
        
        func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
            parent.presentationMode.wrappedValue.dismiss()
        }
    }

    func makeCoordinator() -> Coordinator {
        return Coordinator(self)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<MailView>) -> MFMailComposeViewController {
        vc.mailComposeDelegate = context.coordinator
        return vc
    }

    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {}
}
