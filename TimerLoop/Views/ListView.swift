//
//  ListView.swift
//  Shared
//
//  Created by Lorenzo Lewis on 11/28/20.
//

import SwiftUI

struct ListView: View {
    @Environment(\.managedObjectContext) private var viewContext

    @AppStorage("firstRunWithoutIntro") var firstRunWithoutIntro: Bool = true

    @StateObject private var enabledLoops = LoopFetchRequest(
        context: PersistenceManager.shared.container.viewContext,
        predicate: NSPredicate(format: "isEnabled == true")
    )

    @StateObject private var disabledLoops = LoopFetchRequest(
        context: PersistenceManager.shared.container.viewContext,
        predicate: NSPredicate(format: "isEnabled != true")
    )

    @State private var selectedLoop: Loop?

    var body: some View {
        NavigationView {
            List {

                // Enabled Loops
                ForEach(enabledLoops.loops) { loop in
                    Section {
                        LoopListCellView(loop: loop)
                            .onTapGesture { selectedLoop = loop }
                    }
                }
                .onDelete { indexSet in
                    deleteItems(from: enabledLoops, offsets: indexSet)
                }

                // Add a new loop button
                Section {
                    Button("Create a new loop") {
                        selectedLoop = Loop(context: viewContext)
                    }
                }

                // Disabled Loops
                if disabledLoops.loops.count > 0 {
                    Section(header: Text("Disabled loops")) {
                        ForEach(disabledLoops.loops) { loop in
                            Section {
                                LoopListCellView(loop: loop)
                                    .onTapGesture { selectedLoop = loop }
                            }
                        }
                        .onDelete { indexSet in
                            deleteItems(from: disabledLoops, offsets: indexSet)
                        }
                    }
                }

                // Button to toggle intro button, only shows during development
                #if DEBUG
                Button(action: {
                    firstRunWithoutIntro = true
                }) {
                    Text("Reset intro")
                }
                #endif

            }
            .listStyle(InsetGroupedListStyle())
            .sheet(item: $selectedLoop) { EditLoopView(loop: $0) }
            .navigationTitle("TimerLoop")
            .toolbar {
                ToolbarItem(placement: .primaryAction) {
                    NavigationLink(destination: SettingsView()) {
                        Label("Settings", systemImage: "gear")
                    }
                }
            }
        }
        .sheet(isPresented: $firstRunWithoutIntro) { IntroView() }
    }
}

// MARK:- Functions
extension ListView {
    private func deleteItems(from loopSet: LoopFetchRequest, offsets: IndexSet) {
        withAnimation {
            for i in offsets {
                try? loopSet.loops[i].delete()
            }
        }
    }
}

// MARK:- Preview
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
            .environment(\.managedObjectContext, PersistenceManager.preview.container.viewContext)
    }
}
