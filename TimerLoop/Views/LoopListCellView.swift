//
//  LoopListCellView.swift
//  TimerLoop
//
//  Created by Lorenzo Lewis on 11/29/20.
//

import SwiftUI

struct LoopListCellView: View {

    var loop: Loop
    let weekdays = Calendar.current.veryShortStandaloneWeekdaySymbols
    let secondary = 0.85

    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                
                // Name
                Text(loop.displayName).font(.title2).bold()

                // Time
                HStack(spacing: 5) {
                    Text("From".lowercased()).opacity(secondary)
                    Text(loop.displayStartTime).bold()
                    Text("To".lowercased()).opacity(secondary)
                    Text(loop.displayEndTime).bold()
                }

                // Interval
                HStack(spacing: 5) {
                    Text("Every".lowercased()).opacity(secondary)
                    Text(loop.displayInterval).bold()
                }

                // Days
                HStack(spacing: 5) {
                    Text("on".lowercased()).opacity(secondary)
                    ForEach(0..<weekdays.count) { day in
                        if loop.selectedDays.contains(day) {
                            Text("\(weekdays[day])").bold()
                        } else {
                            Text("\(weekdays[day])").opacity(secondary)
                        }
                    }
                }
            }
            Spacer()
        }
        .foregroundColor(.white)
        .listRowBackground(loop.isEnabled ? Color.accentColor : Color.init(hue: 0, saturation: 0, brightness: 0.15))
        .contentShape(Rectangle()) // Allows for tap area to be anywhere
    }
}

struct LoopListCellView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LoopListCellView(loop: Loop.previewLoop)
                .preferredColorScheme(.light)
                .padding()
                .previewLayout(.sizeThatFits)
            LoopListCellView(loop: Loop.previewLoop)
                .preferredColorScheme(.dark)
                .padding()
                .previewLayout(.sizeThatFits)
            LoopListCellView(loop: Loop.previewLoop)
                .preferredColorScheme(.dark)
                .padding()
                .previewLayout(.sizeThatFits)
                .environment(\.layoutDirection, .rightToLeft)
        }
        .background(Color.blue)
    }
}

