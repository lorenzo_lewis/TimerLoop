//
//  DisableModalDismissViewModifier.swift
//  TimerLoop (iOS)
//
//  Created by Lorenzo Lewis on 12/5/20.
//  Adapted from https://stackoverflow.com/a/60939207
//

import SwiftUI

/// Apply to a view to stop the swipe-down gesture from dismissing the view
struct DisableModalDismiss: ViewModifier {

    let disabled: Bool

    func body(content: Content) -> some View {
        disableModalDismiss()
        return AnyView(content)
    }

    func disableModalDismiss() {
        guard let visibleController = UIApplication.shared.visibleViewController() else { return }
        visibleController.isModalInPresentation = disabled
    }
}

/// Provides support for DisableModalDismiss view modifier
extension UIApplication {

    func visibleViewController() -> UIViewController? {
        guard let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) else { return nil }
        guard let rootViewController = window.rootViewController else { return nil }
        return UIApplication.getVisibleViewControllerFrom(vc: rootViewController)
    }
    
    private static func getVisibleViewControllerFrom(vc: UIViewController) -> UIViewController {

        if let navigationController = vc as? UINavigationController,
           let visibleController = navigationController.visibleViewController {
            return UIApplication.getVisibleViewControllerFrom(vc: visibleController)

        } else if let tabBarController = vc as? UITabBarController,
                  let selectedTabController = tabBarController.selectedViewController {
            return UIApplication.getVisibleViewControllerFrom(vc: selectedTabController)
            
        } else {
            if let presentedViewController = vc.presentedViewController {
                return UIApplication.getVisibleViewControllerFrom(vc: presentedViewController)
            } else {
                return vc
            }
        }
    }
}


