//
//  SettingsView.swift
//  TimerLoop
//
//  Created by Lorenzo Lewis on 12/5/20.
//

import SwiftUI

struct SettingsView: View {

    let viewModel = SettingsViewModel()
    @State private var sheet: AnyView?

    var body: some View {
        Form {

            // Feature request and bug report
            Section {
                Button(action: {
                    sheet = viewModel.submitFeatureRequest()
                }) {
                    Label("Submit feature request", systemImage: "sparkles")
                }
                Button(action: {
                    sheet = viewModel.submitBugReport()
                }) {
                    Label("Report bug", systemImage: "ant.fill")
                }
            }

            // Device settings
            Section {
                Button(action: {
                    viewModel.openSettings()
                }) {
                    Label("Device settings", systemImage: "gear")
                }
            }

            // Website
            Section(footer: HStack {
                Spacer()
                VStack {
                    Image(uiImage: viewModel.appIcon!)
                        .cornerRadius(10)
                    Text("TimerLoop \(viewModel.version) (\(viewModel.build))")
                    Text("Made with ♥ in Kansas City")
                }
                .padding(.top)
                Spacer()
            }) {
                Button(action: {
                    viewModel.openWebsite(url: "https://lorenzolewis.click")
                }) {
                    Label("Developer website", systemImage: "link")
                }
            }
        }
        .sheet(item: $sheet) { $0 }
        .navigationTitle("Settings")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
