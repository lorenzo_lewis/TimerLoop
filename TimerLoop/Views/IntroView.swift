//
//  IntroView.swift
//  TimerLoop (iOS)
//
//  Created by Lorenzo Lewis on 12/27/20.
//

import SwiftUI

struct IntroView: View {

    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        VStack {
            Image(uiImage: Bundle.main.icon!)
                .cornerRadius(10)
            Text("Welcome to")
                .font(.title2)
            Text("TimerLoop")
                .font(.title).bold()
                .padding(.bottom)

            VStack(alignment: .leading) {

                IntroCellView(
                    titleText: "Quick Setup",
                    bodyText: "Set the intervals you'd like to be reminded at and the time range for notifications to be sent.",
                    icon: Image(systemName: "timer"),
                    color: .orange)

                Divider()

                IntroCellView(
                    titleText: "Flexible",
                    bodyText: "Choose which days of the week you'd like your loop to fire. Once a week, weekdays, or even everyday!",
                    icon: Image(systemName: "calendar"),
                    color: .blue)

                Divider()

                IntroCellView(
                    titleText: "Feedback",
                    bodyText: "Have feedback for ways to improve the app? I'd love to hear it! Contact me in Settings.",
                    icon: Image(systemName: "megaphone"),
                    color: .green)
            }
            .padding()
            .background(Color(.secondarySystemGroupedBackground))
            .cornerRadius(20)
            .padding()

            Button(action: {
                presentationMode.wrappedValue.dismiss()
            }) {
                Text("Get Started")
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .background(
                        Capsule()
                    )
            }
        }

    }
}

struct IntroView_Previews: PreviewProvider {
    static var previews: some View {
        IntroView()
            .preferredColorScheme(.dark)
    }
}
