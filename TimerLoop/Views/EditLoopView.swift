//
//  EditLoopView.swift
//  TimerLoop
//
//  Created by Lorenzo Lewis on 11/29/20.
//

import SwiftUI

struct EditLoopView: View {

    @StateObject var loop: Loop
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.colorScheme) var colorScheme
    
    private let stepperSteps: [Double] = {
        var array = Array<Double>(arrayLiteral: 1/60, 2/60, 3/60, 4/60, 5/60, 10/60, 15/60)
        
        while array.last! < 24 {
            array.append(array.last! + 15/60)
        }
        
        return array
    }()

    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Loop Name")) {
                    TextField("Loop name", text: $loop.name).autocapitalization(.words)
                    Toggle("Enable", isOn: $loop.isEnabled).toggleStyle(SwitchToggleStyle(tint: .accentColor))
                }

                Section(
                    header: Text("How often to remind"),
                    footer: Text("Tip: Press and hold -/+ to decrement and increment faster.")
                ) {
                    Stepper(
                        onIncrement: {
                            let currentStep = stepperSteps.firstIndex(of: loop.interval) ?? 1/60
                            if loop.interval != 24 {
                                loop.interval = stepperSteps[currentStep + 1]
                            }
                        },
                        onDecrement: {
                            let currentStep = stepperSteps.firstIndex(of: loop.interval) ?? 1/60
                            if loop.interval != 1/60 {
                                loop.interval = stepperSteps[currentStep - 1]
                            }
                        },
                        label: {
                            Text("Every \(loop.displayInterval)")
                        })
                }

                Section(header: Text("Remind between these times")) {
                    DatePicker("Start Time", selection: $loop.startTime, displayedComponents: .hourAndMinute)
                    DatePicker("End Time", selection: $loop.endTime, displayedComponents: .hourAndMinute)
                }

                Section(header: Text("Days to remind")) {
                    daysRow
                }
            }
            .navigationTitle(loop.displayName)
            .toolbar {
                ToolbarItem(placement: .cancellationAction) {
                    if loop.unsavedChanges {
                        Button("Discard") {
                            presentationMode.wrappedValue.dismiss()
                        }
                    }
                }
                ToolbarItem(placement: .confirmationAction) {
                    if loop.unsavedChanges {
                        Button("Save") {
                            try? loop.save()
                            presentationMode.wrappedValue.dismiss()
                        }
                    } else {
                        Button("Close") {
                            presentationMode.wrappedValue.dismiss()
                        }
                    }
                }
            }
        }
        .modifier(DisableModalDismiss(disabled: loop.unsavedChanges))
    }
}

extension EditLoopView {

    private var days: [String] {
        Calendar.current.veryShortStandaloneWeekdaySymbols
    }

    var daysRow: some View {
        HStack {
            ForEach(0..<days.count) { index in
                Circle()
                    .foregroundColor(
                        loop.selectedDays.contains(index) ? .accentColor : Color(.secondarySystemFill))
                    .overlay(
                        Text(days[index])
                            .foregroundColor(
                                colorScheme == .light && loop.selectedDays.contains(index) ? .white : .primary
                            )
                    )
                    .onTapGesture {
                        loop.modifySelectedDay(index)
                        UISelectionFeedbackGenerator().selectionChanged()
                    }
            }
        }
    }
}

struct EditLoopView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            EditLoopView(loop: Loop.previewLoop)
                .preferredColorScheme(.light)
            EditLoopView(loop: Loop.previewLoop)
                .preferredColorScheme(.dark)
        }
    }
}
