//
//  LoopModel.swift
//  TimerLoop (iOS)
//
//  Created by Lorenzo Lewis on 12/24/20.
//

import Foundation
import CoreData

class Loop: ObservableObject, Identifiable {

    let id: UUID
    @Published var name: String {
        didSet { unsavedChanges = true }
    }
    @Published var isEnabled: Bool {
        didSet { unsavedChanges = true }
    }
    @Published var interval: Double {
        didSet { unsavedChanges = true }
    }
    @Published var startTime: Date {
        didSet { unsavedChanges = true }
    }
    @Published var endTime: Date {
        didSet { unsavedChanges = true }
    }
    @Published var selectedDays: [Int] {
        didSet { unsavedChanges = true }
    }

    var unsavedChanges: Bool = false

    private var coreDataLoop: CoreDataLoop?
    private var context: NSManagedObjectContext

    /// Instantiate a loop using an existing CoreDataLoop
    /// - Parameter coreDataLoop: Existing CoreDataLoop
    init(_ coreDataLoop: CoreDataLoop) {
        id = coreDataLoop.id ?? UUID()
        name = coreDataLoop.name ?? ""
        isEnabled = coreDataLoop.isEnabled
        interval = coreDataLoop.interval
        startTime = coreDataLoop.startTime ?? Date()
        endTime = coreDataLoop.endTime ?? Date().addingTimeInterval(60 * 60) // Adds 1 hour
        selectedDays = coreDataLoop.selectedDays ?? Array(0..<7)

        self.coreDataLoop = coreDataLoop
        context = coreDataLoop.managedObjectContext!

        log.verbose("Loading loop from Core Data")
    }

    /// Instantiate for a draft loop
    /// - Parameter context: Context that the CoreDataLoop should be saved to
    init(context: NSManagedObjectContext) {
        id = UUID()
        name = ""
        isEnabled = true
        interval = 1.0
        startTime = Date()
        endTime = Date()
        selectedDays = Array(0..<7)

        self.context = context
        unsavedChanges = true

        log.verbose("Creating new loop draft")
    }

    /// Used to create a loop from scratch with specific parameters. Used for preview loops.
    internal init(id: UUID, name: String, isEnabled: Bool, interval: Double, startTime: Date, endTime: Date, selectedDays: [Int], coreDataLoop: CoreDataLoop? = nil, context: NSManagedObjectContext) {
        self.id = id
        self.name = name
        self.isEnabled = isEnabled
        self.interval = interval
        self.startTime = startTime
        self.endTime = endTime
        self.selectedDays = selectedDays
        self.coreDataLoop = coreDataLoop
        self.context = context
    }

    /// Attempts to save the modified loop into core data if it already exists or commit a new loop if working with a draft
    func save() throws {
        // Check to see if working with a committed loop or draft loop
        if coreDataLoop == nil {
            coreDataLoop = CoreDataLoop(context: context)
        }

        coreDataLoop?.id = id
        coreDataLoop?.name = name
        coreDataLoop?.isEnabled = isEnabled
        coreDataLoop?.interval = interval
        coreDataLoop?.startTime = startTime
        coreDataLoop?.endTime = endTime
        coreDataLoop?.selectedDays = selectedDays

        log.verbose("Attempting to save loop to Core Data...")
        try context.save()
        NotificationManager.shared.refreshNotifications()
        log.verbose("Saved.")
    }

    func delete() throws {
        guard coreDataLoop != nil else { return }
        context.delete(coreDataLoop!)
        log.verbose("Attempting to delete loop from Core Data...")
        try context.save()
        NotificationManager.shared.refreshNotifications()
        log.verbose("Deleted.")
    }
}

// MARK:- Computed Properties
extension Loop {

    var displayName: String {
        return name != "" ? name : "Loop"
    }

    var displayInterval: String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute]
        formatter.unitsStyle = .full
        if let string = formatter.string(from: interval * 60 * 60) {
            return string
        }
        else {
            return "Error"
        }
    }

    private var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter
    }

    var displayStartTime: String {
        return dateFormatter.string(from: startTime)
    }

    var displayEndTime: String {
        return dateFormatter.string(from: endTime)
    }
}

// MARK:- Functions
extension Loop {
    func modifySelectedDay(_ day: Int) {
        if selectedDays.contains(day) {
            selectedDays.removeAll { $0 == day}
        } else {
            selectedDays.append(day)
        }
    }
}

// MARK:- Preview Loop
extension Loop {
    
    static let previewLoop: Loop = {
        let context = PersistenceManager.preview.container.viewContext
        let selectedDays: [Int] = {
            var days = [Int]()
            for i in 0..<7 {
                if Bool.random() {
                    days.append(i)
                }
            }
            return days
        }()

        return Loop(
            id: UUID(),
            name: "Loop Name",
            isEnabled: true,
            interval: 2.0,
            startTime: Date(),
            endTime: Date().addingTimeInterval(60 * 60),
            selectedDays: selectedDays,
            context: context
        )
    }()
}
