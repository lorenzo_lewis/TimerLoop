//
//  LoopFetchRequest.swift
//  TimerLoop (iOS)
//
//  Created by Lorenzo Lewis on 12/24/20.
//

import Foundation
import CoreData


/// Custom object made to replace @FetchRequest and bypass some of it's limitations. Transforms
/// CoreDataLoop type into a Loop type before offering it as an ObservableObject
class LoopFetchRequest: NSObject, ObservableObject {

    @Published var loops: [Loop] = []

    private var coreDataLoops: [CoreDataLoop] = []
    private let controller: NSFetchedResultsController<CoreDataLoop>


    /// Initialize a new LoopFetchRequest
    /// - Parameters:
    ///   - context: Context to fetch from
    ///   - predicate: Custom predicate to filter on
    init(context: NSManagedObjectContext, predicate: NSPredicate) {

        let fetchRequest: NSFetchRequest<CoreDataLoop> = CoreDataLoop.fetchRequest()

        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = []
        
        controller = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: context,
            sectionNameKeyPath: nil,
            cacheName: nil
        )

        super.init()

        controller.delegate = self

        do {
            try controller.performFetch()
            coreDataLoops = controller.fetchedObjects ?? []

            // Convert the retrieved loops to a Loop type and assigns to array
            loops = coreDataLoops.compactMap { Loop($0) }
        } catch {
            log.error(error)
        }
    }
}

/// Detects any changes in the store and refreshes the fetch request
extension LoopFetchRequest: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {

        guard let results = controller.fetchedObjects as? [CoreDataLoop]
        else { return }

        coreDataLoops = results
        loops = results.compactMap { Loop($0) }

    }
}
