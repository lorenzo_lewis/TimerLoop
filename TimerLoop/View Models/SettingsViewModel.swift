//
//  SettingsViewModel.swift
//  TimerLoop
//
//  Created by Lorenzo Lewis on 12/5/20.
//

import UIKit
import SwiftUI
import MessageUI

struct SettingsViewModel {

    let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
    let build = Bundle.main.infoDictionary?["CFBundleVersion"] as! String
    let appIcon = Bundle.main.icon

    /// Gathers log files and attaches to an email. If email is not set up then it falls back to an ActivityView
    func submitBugReport() -> AnyView? {
        // Get log file
        guard let logFile = LoggingManager.shared.retrieveLogs() else { return nil }

        let fm = FileManager()
        var destinationUrl = fm.temporaryDirectory

        // Date for filename
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddhhmmss"
        let date = dateFormatter.string(from: Date())

        // Construct file pathes
        let filename = "TimerLoop\(date).log"
        destinationUrl = destinationUrl.appendingPathComponent(filename)

        // Attempts to copy file
        do {
            try fm.copyItem(at: logFile, to: destinationUrl)
        } catch {
            log.error("Not able to copy file: \(error)")
            return nil
        }

        log.info("Log file retrieved and copied.")

        // Check to see if an email can be sent and that data was able to be retrieved from log file
        if MFMailComposeViewController.canSendMail(), let data = try? Data(contentsOf: destinationUrl) {

            log.info("Setting up MailView")

            let recipients = ["incoming+lorenzo-lewis-timerloop-22901230-bkr0nxpazzamwb2grag9oh7u-issue@incoming.gitlab.com"]
            let subject = "TimerLoop Bug Report"
            let messageBody = """
            Describe the issue that occured:<br>
            What steps can you take to reproduce it?<br>
            <p>Version: \(version) (\(build))</p>
            """

            // Create the view controller and set it up
            let vc = MFMailComposeViewController()
            vc.setToRecipients(recipients)
            vc.setSubject(subject)
            vc.addAttachmentData(data, mimeType: "text/plain", fileName: filename)
            vc.setMessageBody(messageBody, isHTML: true)

            return AnyView(MailView(vc: vc))

        } else {
            // Can't send an email, display action sheet instead
            log.info("Email client was not configured, presenting ActivityView.")
            let vc = UIActivityViewController(activityItems: [destinationUrl], applicationActivities: nil)

            return AnyView(ActivityView(vc: vc))
        }
    }


    /// Attempts to display an email composer. Shows an alert if email is not set up.
    func submitFeatureRequest() -> AnyView? {
        // Check if email can be sent
        if MFMailComposeViewController.canSendMail() {

            let recipients = ["incoming+lorenzo-lewis-timerloop-22901230-bkr0nxpazzamwb2grag9oh7u-issue@incoming.gitlab.com"]
            let subject = "TimerLoop Feature Request"
            let messageBody = """
            Please describe the feature you have in mind:<br>
            <br>Version: \(version) (\(build))
            """

            let vc = MFMailComposeViewController()
            vc.setToRecipients(recipients)
            vc.setSubject(subject)
            vc.setMessageBody(messageBody, isHTML: true)

            return AnyView(MailView(vc: vc))
        } else {
            // Can't send an email, display an alert instead
            let alert = Alert(
                title: Text("Email not setup"),
                message: Text("It looks like you don't have email set up on your device. Feel free to send your feature request to timerloop@lorenzolewis.click."),
                dismissButton: .default(Text("Close"))
            )
            AlertManager.shared.enqueue(alert)

            return nil
        }
    }

    func openSettings() {
        let settingsUrl = URL(string: UIApplication.openSettingsURLString)
        UIApplication.shared.open(settingsUrl!)
    }

    func openWebsite(url: String) {
        guard let url = URL(string: url) else { return }
        UIApplication.shared.open(url)
    }
}

/// Get app icon from bundle. Would normally place this in a separate extensions file, but leaving here
/// Since there are no other extensions for Bundle and minimal usage.
extension Bundle {
    public var icon: UIImage? {
        if let icons = infoDictionary?["CFBundleIcons"] as? [String: Any],
           let primaryIcon = icons["CFBundlePrimaryIcon"] as? [String: Any],
           let iconFiles = primaryIcon["CFBundleIconFiles"] as? [String],
           let lastIcon = iconFiles.last {
            return UIImage(named: lastIcon)
        }
        return nil
    }
}

// Allows to use a sheet to present a view
extension AnyView: Identifiable {
    public var id: UUID {
        UUID()
    }
}
