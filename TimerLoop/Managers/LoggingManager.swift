//
//  LoggingManager.swift
//  TimerLoop
//
//  Created by Lorenzo Lewis on 11/30/20.
//

import Foundation
import SwiftyBeaver

/// Singleton that allows logging messages from anywhere in the app. 5 different levels of logs are available:
/// Verbose, Debug, Info, Warning, and Error
///
/// SwiftyBeaver docs: https://docs.swiftybeaver.com
///
/// *Note* Planning to update to native OSLog once log exporting is supported in iOS. SwiftyBeaver's library
/// provides the closest functionality to OSLog that will allow for minimal changes in the future to de-scope
/// SwiftyBeaver. Keeping an eye on https://github.com/ole/OSLogStoreTest for updates.
struct LoggingManager {

    static let shared = LoggingManager()

    let log = SwiftyBeaver.self

    private init() {
        let file = FileDestination()
        file.minLevel = .warning
        let console = ConsoleDestination()
        log.addDestination(file)
        log.addDestination(console)
    }

    /// Locations the log file on disk and returns the URL of it if available
    func retrieveLogs() -> URL? {
        let cacheDirectory = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        guard let file = cacheDirectory.first?.appendingPathComponent("swiftybeaver.log") else {
            log.error("Not able to locate the log files to retrieve")
            return nil
        }

        return file
    }
}
