//
//  VersionManager.swift
//  TimerLoop
//
//  Created by Lorenzo Lewis on 12/1/20.
//

import Foundation

/// Keeps track of the version of the app installed and writes to User Defaults.
/// If there are features in future versions that need to check for a previous version
/// and perform an upgrade, this provides a baseline to support migration.
struct VersionManager {

    let runningVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String

    @discardableResult init() {

        guard let storedVersion = UserDefaults.standard.string(forKey: "version") else {
            log.info("No previous version found, saving \(String(describing: runningVersion))")
            UserDefaults.standard.setValue(runningVersion, forKey: "version")
            return
        }

        log.info("Found version \(storedVersion)")

        // Perform any migrations that are needed here based on version found.
    }
}
