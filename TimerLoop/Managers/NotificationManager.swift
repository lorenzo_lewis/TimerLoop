//
//  NotificationManager.swift
//  TimerLoop
//
//  Created by Lorenzo Lewis on 11/29/20.
//

// https://stackoverflow.com/questions/30852870/displaying-a-stock-ios-notification-banner-when-your-app-is-open-and-in-the-fore/40756060#40756060

import SwiftUI
import UserNotifications
import CoreData

/// Singleton that reads in all loops from Core Data and then sets up Push Notifications if enabled, or sets up
/// in-app alerts as a fallback if not.
struct NotificationManager {

    static let shared = NotificationManager()
    let center = UNUserNotificationCenter.current()

    private struct NotificationData {
        let title: String
        var components: DateComponents
        let threadIdentifier: String
    }

    private init() {}

    /// Reads all loops and schedules notifications/alerts accordingly
    func refreshNotifications() {

        log.info("Attempting to refresh the notifications...")
        guard let loops = fetchCoreDataLoops() else {
            log.error("Problem fetching loops from core data")
            return
        }

        let notifications = createNotificationData(from: loops)

        center.removeAllPendingNotificationRequests()

        if notifications.count > 0 {
            schedule(with: notifications)
        }
    }

    /// Read in loops from Core Data and then returns an array if successful
    private func fetchCoreDataLoops() -> [CoreDataLoop]? {

        let context = PersistenceManager.shared.container.viewContext
        let fetchRequest: NSFetchRequest<CoreDataLoop> = CoreDataLoop.fetchRequest()
        let loops = try? context.fetch(fetchRequest)
        return loops
    }

    /// Converts Core Data loops and returns a type that allows for easier creation of notifications and alerts
    /// based off of DateComponents
    private func createNotificationData(from loops: [CoreDataLoop]) -> [NotificationData] {

        var notifications = [NotificationData]()

        // Filter down to loops that are enabled
        let loops = loops.filter { $0.isEnabled }

        for loop in loops {

            // Fallback to all days if none are selected due to the previous data model
            let days = loop.selectedDays ?? Array(0..<7)

            // Setup alerts for each day
            for day in days {

                // Makes sure that startTime and endTime are non-nil
                guard var startTime = loop.startTime,
                      var endTime = loop.endTime else { continue }

                // Set start and end times to current date
                var startTimeComponents = Calendar.current.dateComponents([.hour, .minute], from: startTime)
                startTimeComponents.weekday = day + 1
                startTime = Calendar.current.nextDate(after: Date(), matching: startTimeComponents, matchingPolicy: .nextTime)!

                var endTimeComponents = Calendar.current.dateComponents([.hour, .minute], from: endTime)
                endTimeComponents.weekday = day + 1
                endTime = Calendar.current.nextDate(after: Date(), matching: endTimeComponents, matchingPolicy: .nextTime)!

                // If end time is prior to start time, move ahead a day (past midnight)
                if endTime < startTime {
                    endTime = Calendar.current.date(byAdding: .day, value: 1, to: endTime)!
                }

                // Create notification object with base data
                var notification = NotificationData(
                    title: loop.name ?? "Your timer has looped!",
                    components: startTimeComponents,
                    threadIdentifier: loop.id!.uuidString
                )

                // Add the set interval to the start time to build out notifications for the day
                while startTime <= endTime {
                    startTimeComponents = Calendar.current.dateComponents([.hour, .minute, .weekday], from: startTime)

                    notification.components = startTimeComponents
                    notifications.append(notification)

                    // Add the interval to the start time
                    startTime = Calendar.current.date(byAdding: .minute, value: Int(loop.interval * 60), to: startTime)!
                }
            }
        }

        log.info("Created \(notifications.count) notification data to try to schedule")
        return notifications
    }

    /// Checks if notification permissions have been granted and ask user if not. If permission is denied
    /// then set up alerts to show in app as a fallback.
    private func schedule(with notifications: [NotificationData]) {

        log.info("Checking notification authorization status...")

        center.getNotificationSettings { settings in
            switch settings.authorizationStatus {
            case .notDetermined:
                // Request authorization
                log.info("Requesting notification authorization...")
                center.requestAuthorization(options: [.alert, .sound]) { granted, error in
                    if granted && error == nil {
                        log.info("Granted.")
                        scheduleNotifications(with: notifications)
                    } else {
                        log.info("Not granted: \(granted) \(String(describing: error))")
                        scheduleAlerts(with: notifications)
                    }
                }
            case .authorized, .provisional, .ephemeral:
                log.info("Notifications are allowed.")
                scheduleNotifications(with: notifications)
            case .denied:
                log.info("Notifications are denied.")
                scheduleAlerts(with: notifications)
            default:
                log.error("An issue occurred while checking for notification authorization status: \(settings.authorizationStatus)")
                scheduleAlerts(with: notifications)
            }
        }
    }

    /// Schedules push notifications based on the NotificationData passed in.
    private func scheduleNotifications(with notifications: [NotificationData]) {

        log.info("Attempting to schedule \(notifications.count) notification(s)...")

        for notification in notifications {

            let content = UNMutableNotificationContent()
            content.title = notification.title
            content.threadIdentifier = notification.threadIdentifier

            let trigger = UNCalendarNotificationTrigger(dateMatching: notification.components, repeats: true)
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)

            center.add(request) { error in
                if let error = error {
                    log.error("Issue adding notification request to UserNotificationCenter: \(error)")
                }
            }
        }
        log.info("Complete.")
    }

    /// Schedules in-app alerts. This will filter down to the next notification that should be fired and then
    /// add that to the AlertManager queue. Upon alert, it will trigger another check to determine if an
    /// additional alert should be fired.
    private func scheduleAlerts(with notifications: [NotificationData]) {

        log.info("Scheduling alerts.")

        // Sort by dates, getting next alert that should occur
        let notifications = notifications.sorted {
            let firstDate = Calendar.current.nextDate(after: Date(), matching: $0.components, matchingPolicy: .nextTime)!
            let secondDate = Calendar.current.nextDate(after: Date(), matching: $1.components, matchingPolicy: .nextTime)!

            return firstDate < secondDate
        }

        guard let nextNotification = notifications.first else {
            log.info("No notifications to create an alert for")
            return
        }

        // Date to schedule the timer for
        let date = Calendar.current.nextDate(after: Date(), matching: nextNotification.components, matchingPolicy: .nextTime)!

        // Set up a timer to fire at the specified date that will show the alert.
        let timer = Timer(fire: date, interval: 0, repeats: false) { _ in
            log.info("Firing in-app timer for alert notification")

            let alert = Alert(title: Text(nextNotification.title), message: nil, dismissButton: .default(Text("Dismiss")))
            AlertManager.shared.enqueue(alert)

            // Check for the next notification
            refreshNotifications()
        }

        // Add the timer to the runloop. This may not fire at the precise time
        // scheduled, but it should be more than accurate enough for this app
        RunLoop.main.add(timer, forMode: .common)
    }


}
