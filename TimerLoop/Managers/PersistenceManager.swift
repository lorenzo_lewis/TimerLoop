//
//  PersistenceManager.swift
//  Shared
//
//  Created by Lorenzo Lewis on 11/28/20.
//

import CoreData

/// Singleton as provided by Apple for managing Core Data persistent container. CloudKit has been set up
/// but is not enabled.
struct PersistenceManager {
    static let shared = PersistenceManager()

    static var preview: PersistenceManager = {
        let result = PersistenceManager(inMemory: true)
        let viewContext = result.container.viewContext
        for _ in 0..<3 {
            let newLoop = CoreDataLoop(context: viewContext)
            newLoop.isEnabled = true
            newLoop.id = UUID()
        }
        do {
            try viewContext.save()
        } catch {
            log.error("An issue occurred while attempting to save the preview loops: \(error)")
        }
        return result
    }()

    let container: NSPersistentCloudKitContainer

    private init(inMemory: Bool = false) {
        container = NSPersistentCloudKitContainer(name: "TimerLoop")
        if inMemory {
            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
        }
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                log.error("Something really bad happened when trying to load the CoreData persistent store: \(error)")
            }
        })
    }
}
