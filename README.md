# TimerLoop: Repeatable Timers

**Set looping timers to remind yourself of repetitive tasks throughout the day.**

 *Focusing on work and need to be reminded to check your email or to take a break? Set a loop to checkin every 15 minutes.*

 *Stressed out and want to remember to take a breath? Set a loop for that and get some calm.*

<br>

## iOS Frameworks and Technologies Used
- **SwiftUI:** Overall UI for the app
- **Combine:** Used to create a custom bridge between Core Data and views
- **UserNotifications:** Notifies the user for the time they set; falls back to in-app alerts to meet App Store guidelines to not require notifications for the app to work
- **CoreData:** All data is stored and synced. Not required for the small size of data the app uses, but allowed me to gain experience
- **UIKit & MessageKit:** Worked as a bridge from SwiftUI to allow presenting activity and mail views to share logs
- **[SwiftyBeaver](https://github.com/SwiftyBeaver/SwiftyBeaver):** Logging that is built into iOS currently does not allow for easy log extraction programatically. Using this framework until the functionality is added natively as SwiftyBeaver follows OSLog most closely and allows for an easy transition in the future.
- **[Fastlane](https://github.com/fastlane/fastlane):** Used to automate build version incrementing, building and uploading to App Store Connect, and releasing on Test Flight.

## Future Considerations
- Accessibility and Localization
- Use UITableView instead of SwiftUI so that I can fix a few graphical glitches I've encountered and for greater flexibility
- Different types of loops based on user feedback
- Ability to change notification sounds and images for additional user customization

<br>

<img src="Assets/launch.png" alt="Launch screen"
	title="Opening screen of TimerLoop" width="200" />
<img src="Assets/list.png" alt="List of current loops"
	title="List of current loops" width="200" />
<img src="Assets/edit.png" alt="Edit view of a loop"
	title="Edit view of a loop" width="200" />
